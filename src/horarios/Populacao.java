package horarios;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hyago
 */
public class Populacao {

    private List<Cromossomo> populacao;
    private List<Cromossomo> novaPopulacao;
    private int tamanho;
    private int somaAvaliacoes;
    private Cromossomo cromossomo;

    public Populacao() {
        this.populacao = new ArrayList<>();
        this.somaAvaliacoes = 0;
    }
    
    public void executa(List<Periodo> periodos, int tamanho, int numGeracoes) {
        int i;
        this.tamanho = tamanho;
        this.iniciarPopulacao(periodos);
        
        for (i = 0; i < numGeracoes; i++) {
            System.out.println("Geracao " + i + "\n");
            this.geracao();
            this.moduloPopulacao();
        }
        
        i = this.determinaMelhor();
        System.out.println("Melhor indivíduo: ");
	this.populacao.get(i).imprimir();
    }

    public void iniciarPopulacao(List<Periodo> periodos) {
        
        for (int i = 0; i < getTamanho(); i++) {
            
            System.out.println("======== Cromossomo " + i);
            
            this.cromossomo = new Cromossomo(periodos);
            this.cromossomo.permutar();
            populacao.add(this.cromossomo);
        }
    }

    private double calculaSomaAvaliacoes() {
        somaAvaliacoes = 0;
        
        for (Cromossomo cromossomo : populacao) {
            somaAvaliacoes += cromossomo.getRegras().getPontosCromossomo();
        }
        
        System.out.println("Soma da avaliacoes: " + somaAvaliacoes);
        return somaAvaliacoes;
    }

    public void geracao() {
        this.novaPopulacao = new ArrayList<>();
        Cromossomo pai1;
        Cromossomo pai2;
        Cromossomo filho;
        System.out.println("Calculando nova geracao...\n");
        
        calculaSomaAvaliacoes();

        for (int i = 0; i < this.getTamanho(); i++) {
            
            //System.out.println("########################################");
            System.out.println("\tCRUZAMENTO " + (i + 1) + "\n");

            pai1 = populacao.get(this.roleta());
            pai2 = populacao.get(this.roleta());

            filho = pai1.cruzamento(pai2);
            System.out.println("\tFilho " + i);
            filho.imprimir();

            if (filho.mutacao(this.populacao.size(), 1)) {
                //filho.imprimir();
            }

            this.novaPopulacao.add(filho);
            
            //novaPopulacao.get(i).imprimir();
            //System.out.println("########################################\n");
        }
    }
    
    public int roleta() {
        int posicao;
        double total = 0;
        double limite = Math.random() * (this.somaAvaliacoes + 1);

        //System.out.println("Limite: " + limite);

        for (posicao = 0; ((posicao < this.getTamanho()) && (total < limite)); posicao++) {
            total += populacao.get(posicao).getRegras().getPontosCromossomo();
        }
        
        if(posicao == getTamanho()) {
            posicao--;
        }

        //System.out.println("Pai escolhido: " + posicao);
        return posicao;
    }
    
    public void moduloPopulacao() {
        populacao = new ArrayList<Cromossomo>();
        
        for (Cromossomo cromossomo : getNovaPopulacao()) {
            populacao.add(cromossomo);
        }
        
        novaPopulacao = new ArrayList<Cromossomo>();
    }

    private int determinaMelhor() {
        int i; 
        int melhorCromossomo = 0;
        Cromossomo auxiliar;
        double melhorAvaliado = populacao.get(0).getRegras().getPontosCromossomo();
        
        for (i = 1; i < this.populacao.size(); i++) {
            auxiliar = this.populacao.get(i);
            
            if (auxiliar.getRegras().getPontosCromossomo() > melhorAvaliado) {
                melhorAvaliado = auxiliar.getRegras().getPontosCromossomo();
                melhorCromossomo = i;
            }
        }
        return melhorCromossomo;
    }

    public void imprimir() {
        for (Cromossomo cromossomo : getPopulacao()) {
            cromossomo.imprimir();
        }
    }

    public List<Cromossomo> getPopulacao() {
        return populacao;
    }

    public void setPopulacao(List<Cromossomo> populacao) {
        this.populacao = populacao;
    }

    public void setNovaPopulacao(List<Cromossomo> novaPopulacao) {
        this.novaPopulacao = novaPopulacao;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    public void setSomaAvaliacoes(int somaAvaliacoes) {
        this.somaAvaliacoes = somaAvaliacoes;
    }

    public List<Cromossomo> getNovaPopulacao() {
        return novaPopulacao;
    }

    public int getTamanho() {
        return tamanho;
    }

    public int getSomaAvaliacoes() {
        return somaAvaliacoes;
    }
}
