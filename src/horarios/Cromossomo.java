package horarios;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hyago
 */
public class Cromossomo {

    private List<Periodo> periodos;
    private Regras regras;

    public Cromossomo(List<Periodo> periodos) {
        this.regras = new Regras(this);
        criandoPeriodos(this, periodos);
    }
    
    public void criandoPeriodos(Cromossomo cromossomo, List<Periodo> periodos) {
        Periodo auxiliar;
        
        cromossomo.periodos = new ArrayList<>();
        
        for (int i = 0; i < periodos.size(); i++) {
            auxiliar = new Periodo();
            
            for (int j = 0; j < periodos.get(i).getTurmas().size(); j++) {
                auxiliar.setTurmas(periodos.get(i).getTurmas());
            }
            
            cromossomo.periodos.add(auxiliar);
        }
    }

    public void permutar() {
        int linha;
        int coluna;

        for (int periodo = 0; periodo < this.periodos.size(); periodo++) {

            for (int disciplinas = 0; disciplinas < this.periodos.get(periodo).getTurmas().size(); disciplinas++) {

                for (int aula = 0; aula < this.periodos.get(periodo).getTurmas().get(disciplinas).getAulasPorSemana(); aula++) {
                    inserirAleatorio(this, this.periodos.get(periodo).getTurmas().get(disciplinas), periodo);
                }
            }
        }
    }

    public int aulasTotal() {
        int aulas = 0;

        for (int periodo = 0; periodo < this.periodos.size(); periodo++) {

            for (int disciplinas = 0; disciplinas < this.periodos.get(periodo).getTurmas().size(); disciplinas++) {
                aulas += this.periodos.get(periodo).getTurmas().get(disciplinas).getAulasPorSemana();
            }
        }

        return aulas;
    }

    public void inserirAleatorio(Cromossomo cromossomo, Turma turma, int periodo) {
        int linha;
        int coluna;
        boolean saida = false;

        while (saida == false) {

            linha = (int) (Math.random() * 6);
            coluna = (int) (Math.random() * 5);
            
            if (cromossomo.periodos.get(periodo).getHorario()[linha][coluna] == null) {

                cromossomo.periodos.get(periodo).getHorario()[linha][coluna] = turma;
                saida = true;
            }
        }

        this.regras.avaliacao();
    }

    public Cromossomo cruzamento(Cromossomo cromossomo) {
        Cromossomo filho = new Cromossomo(periodos);
        int sorteio;
        int turma = 0;

        for (int periodo = 0; periodo < this.periodos.size(); periodo++) {

            for (int disciplinas = 0; disciplinas < cromossomo.periodos.get(periodo).getTurmas().size(); disciplinas++) {

                filho.periodos.get(periodo).setTurmas(this.periodos.get(periodo).getTurmas());

                for (int aulas = 0; aulas < cromossomo.periodos.get(periodo).getTurmas().get(turma).getAulasPorSemana(); aulas++) {

                    sorteio = (int) (Math.random() * 2);

                    if (sorteio == 1) {
                        paiEscolhido(this, cromossomo, filho, turma, aulas, periodo);
                    } else {
                        paiEscolhido(cromossomo, this, filho, turma, aulas, periodo);
                    }
                }
                turma++;
            }
        }
        filho.regras.avaliacao();
        return filho;
    }

    public void paiEscolhido(Cromossomo paiEscolhido, Cromossomo paiReserva, Cromossomo filho, int turma, int aulas, int periodo) {
        int linha;
        int coluna;
        boolean inserido;

        linha = posicaoAula(paiEscolhido, 1, turma, aulas, periodo);
        coluna = posicaoAula(paiEscolhido, 2, turma, aulas, periodo);

        inserido = geneEscolhido(paiEscolhido, filho, linha, coluna, periodo);

        if (inserido == false) {
            inserido = geneEscolhido(paiReserva, filho, linha, coluna, periodo);

            if (inserido == false) {
                inserirAleatorio(filho, paiEscolhido.periodos.get(periodo).getTurmas().get(turma), periodo);
            }
        }
    }

    public int posicaoAula(Cromossomo cromossomo, int direcao, int turma, int aula, int periodo) {
        int replicada = -1;

        for (int linha = 0; linha < 6; linha++) {

            for (int coluna = 0; coluna < 5; coluna++) {

                if (cromossomo.periodos.get(periodo).getHorario()[linha][coluna]
                        == cromossomo.periodos.get(periodo).getTurmas().get(turma)) {
                    replicada++;

                    if (aula == replicada) {

                        if (direcao == 1) {
                            return linha;
                        } else {
                            return coluna;
                        }
                    }
                }
            }
        }
        return 0;
    }

    public boolean geneEscolhido(Cromossomo pai, Cromossomo filho, int linha, int coluna, int periodo) {

        if (filho.periodos.get(periodo).getHorario()[linha][coluna] == null) {
            filho.periodos.get(periodo).getHorario()[linha][coluna] = pai.periodos.get(periodo).getHorario()[linha][coluna];
            return true;
        } else {
            return false;
        }
    }

    public boolean mutacao(int populacao, double taxaMutacao) {

        if (((Math.random() * (populacao + 1))) <= ((populacao * taxaMutacao) / 100)) {
            Turma troca = new Turma();
            int origem = (int) ((Math.random() * 30));
            int destino = (int) ((Math.random() * 30));
            int periodo = (int) ((Math.random() * this.periodos.size()));

            //System.out.println("---------- Mutação -----------");
            troca = this.periodos.get(periodo).getHorario()[origem % 6][(origem / 6)];

            this.periodos.get(periodo).getHorario()[origem % 6][(origem / 6)] = this.periodos.get(periodo).getHorario()[destino % 6][(destino / 6)];

            this.periodos.get(periodo).getHorario()[destino % 6][(destino / 6)] = troca;

            /*System.out.print("Origem: " + (origem + 1));
             System.out.println("\tDestino: " + (destino + 1));
             System.out.print("Origem: " + ((origem % 6) + 1) + " " + ((origem / 6) + 1));
             System.out.println("\tDestino: " + ((destino % 6) + 1) + " " + ((destino / 6) + 1) + "\n");*/
            this.regras.avaliacao();

            return true;
        }

        return false;
    }

    public void imprimir() {
        int horario = 0;

        System.out.println("==============================");
        System.out.println("Seg  Ter  Qua  Qui  Sex  | Hor");
        System.out.println("==============================");

        for (int linha = 0; linha < 6; linha++) {
            horario++;

            if (linha == 3) {
                System.out.println("------------------------------");
                horario = 1;
            }

            for (int periodo = 0; periodo < this.periodos.size(); periodo++) {

                for (int coluna = 0; coluna < 5; coluna++) {

                    if (this.periodos.get(periodo).getHorario()[linha][coluna] == null) {
                        System.out.print("---  ");
                    } else {
                        System.out.print(this.periodos.get(periodo).getHorario()[linha][coluna].getDisciplina() + " ");
                    }
                }
                System.out.println("| " + (horario) + "º");
            }
        }
        System.out.println("------------------------------");
    }

    public List<Periodo> getPeriodos() {
        return periodos;
    }

    public void setPeriodos(List<Periodo> periodos) {
        this.periodos = periodos;
    }

    public Regras getRegras() {
        return regras;
    }
}
