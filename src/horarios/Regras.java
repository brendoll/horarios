package horarios;

/**
 *
 * @author hyago
 */
public class Regras {

    private Cromossomo cromossomo;
    private int pontosCromossomo;

    public Regras(Cromossomo cromossomo) {
        this.cromossomo = cromossomo;
    }

    public void avaliacao() {
        this.pontosCromossomo = this.cromossomo.aulasTotal();
        //System.out.println("Total  : " + this.pontosCromossomo);

        this.pontosCromossomo -= aulasNoDia();
        //System.out.println("Parcial: " + this.pontosCromossomo);

        this.pontosCromossomo -= intervaloEntreAulas();
        //System.out.println("Final  : " + this.pontosCromossomo);
    }

    public int aulasNoDia() {
        int punicao = 0;
        
        for (int periodo = 0; periodo < this.cromossomo.getPeriodos().size(); periodo++) {
            
            for (int coluna = 0; coluna < 5; coluna++) {

                for (int linha = 0; linha < 6; linha++) {

                    if (this.cromossomo.getPeriodos().get(periodo).getHorario()[linha][coluna] != null) {

                        for (int dias = (linha + 1); dias < 6; dias++) {

                            if (this.cromossomo.getPeriodos().get(periodo).getHorario()[dias][coluna]
                                    == this.cromossomo.getPeriodos().get(periodo).getHorario()[linha][coluna]) {
                                punicao++;
                            }
                        }
                    }
                }
            }
        }
        return punicao;
    }

    public int intervaloEntreAulas() {
        int punicao = 0;

        for (int periodo = 0; periodo < this.cromossomo.getPeriodos().size(); periodo++) {
            
            for (int coluna = 0; coluna < 5; coluna++) {

                if ((this.cromossomo.getPeriodos().get(periodo).getHorario()[1][coluna] == null)
                        && (this.cromossomo.getPeriodos().get(periodo).getHorario()[0][coluna] != null)
                        && (this.cromossomo.getPeriodos().get(periodo).getHorario()[2][coluna] != null)) {
                    punicao++;
                }

                if ((this.cromossomo.getPeriodos().get(periodo).getHorario()[4][coluna] == null)
                        && (this.cromossomo.getPeriodos().get(periodo).getHorario()[3][coluna] != null)
                        && (this.cromossomo.getPeriodos().get(periodo).getHorario()[5][coluna] != null)) {
                    punicao++;
                }
            }
        }    
        return punicao;
    }

    public void setCromossomo(Cromossomo cromossomo) {
        this.cromossomo = cromossomo;
    }

    public void setPontosCromossomo(int pontosCromossomo) {
        this.pontosCromossomo = pontosCromossomo;
    }

    public Cromossomo getCromossomo() {
        return cromossomo;
    }

    public int getPontosCromossomo() {
        return pontosCromossomo;
    }
}
