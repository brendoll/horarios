package horarios;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hyago
 */
public class Periodo {

    private List<Turma> turmas;
    private Turma horario[][];

    public Periodo() {
        this.horario = new Turma[6][5];
        this.turmas = new ArrayList<>();
    }

    public Periodo(List<Turma> turmas) {
        this.horario = new Turma[6][5];
        this.turmas = new ArrayList<>();
        this.turmas = turmas;
    }

    public void setTurmas(List<Turma> turmas) {
        this.turmas = turmas;
    }

    public void setHorario(Turma[][] horario) {
        this.horario = horario;
    }

    public List<Turma> getTurmas() {
        return turmas;
    }

    public Turma[][] getHorario() {
        return horario;
    }
}
