package horarios;

/**
 *
 * @author hyago
 */
public class Preferencia {

    private int turno;
    private int dia;

    public Preferencia(int linha, int coluna) {
        this.turno = linha;
        this.dia = coluna;
    }

    public void setTurno(int turno) {
        this.turno = turno;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getTurno() {
        return turno;
    }

    public int getDia() {
        return dia;
    }

}
