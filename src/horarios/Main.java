package horarios;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hyago
 */
public class Main {

    public static void main(String[] args) {
        List<Periodo> periodos = new ArrayList<>();
        
        Periodo periodo1 = new Periodo();
        Periodo periodo2 = new Periodo();
        
        List<Turma> turmas1 = new ArrayList<>();
        List<Turma> turmas2 = new ArrayList<>();

        Professor prof1 = new Professor("Prof1");
        Professor prof2 = new Professor("Prof2");
        Professor prof3 = new Professor("Prof3");
        Professor prof4 = new Professor("Prof4");
        Professor prof5 = new Professor("Prof5");
        Professor prof6 = new Professor("Prof6");

        Turma t1 = new Turma(prof1, "ALG ", 3);
        Turma t2 = new Turma(prof2, "LOG ", 2);
        Turma t3 = new Turma(prof3, "IIN ", 2);
        Turma t4 = new Turma(prof4, "TGA ", 2);
        Turma t5 = new Turma(prof5, "MAT ", 2);
        Turma t6 = new Turma(prof6, "OP1 ", 2);

        Turma t7 = new Turma(prof1, "POO ", 2);
        Turma t8 = new Turma(prof2, "FSM ", 3);
        Turma t9 = new Turma(prof3, "ALL ", 2);
        Turma t10 = new Turma(prof4, "OSM ", 2);
        Turma t11 = new Turma(prof5, "EDA ", 2);
        Turma t12 = new Turma(prof6, "OP2 ", 2);
        
        turmas1.add(t1);
        turmas1.add(t2);
        turmas1.add(t3);
        turmas1.add(t4);
        turmas1.add(t5);
        turmas1.add(t6);
        
        turmas2.add(t7);
        turmas2.add(t8);
        turmas2.add(t9);
        turmas2.add(t10);
        turmas2.add(t11);
        turmas2.add(t12);
        
        Preferencia nenhuma = new Preferencia(-1, -1);
        
        Preferencia segunda = new Preferencia(-1, 0);
        Preferencia terca = new Preferencia(-1, 1);
        Preferencia quarta = new Preferencia(-1, 2);
        Preferencia quinta = new Preferencia(-1, 3);
        Preferencia sexta = new Preferencia(-1, 4);
        
        Preferencia manha1 = new Preferencia(0, -1);
        Preferencia manha2 = new Preferencia(1, -1);
        Preferencia manha3 = new Preferencia(2, -1);
        
        Preferencia tarde1 = new Preferencia(0, -1);
        Preferencia tarde2 = new Preferencia(1, -1);
        Preferencia tarde3 = new Preferencia(2, -1);
   
        //prof1.getPreferencias().add(nenhuma);
        //prof2.getPreferencias().add(nenhuma);
        //prof3.getPreferencias().add(nenhuma);
        //prof4.getPreferencias().add(nenhuma);
        //prof5.getPreferencias().add(nenhuma);
        //prof6.getPreferencias().add(nenhuma);
        
        periodo1.setTurmas(turmas1);
        
        periodos.add(periodo1);
        
        //Cromossomo cromossomo = new Cromossomo(periodos);
        //cromossomo.permutar();
        //cromossomo.imprimir();
        
        Populacao populacao = new Populacao();
        populacao.executa(periodos, 100, 1);
    }
}
