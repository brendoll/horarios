package horarios;

/**
 *
 * @author hyago
 */
public class Turma {

    private Professor professor;
    private String disciplina;
    private int aulasPorSemana;
    
    public Turma() {
        
    }

    public Turma(Professor professor, String disciplina, int aulasPorSemana) {
        this.professor = professor;
        this.disciplina = disciplina;
        this.aulasPorSemana = aulasPorSemana;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }

    public void setAulasPorSemana(int aulasPorSemana) {
        this.aulasPorSemana = aulasPorSemana;
    }

    public Professor getProfessor() {
        return professor;
    }

    public int getAulasPorSemana() {
        return aulasPorSemana;
    }

    public String getDisciplina() {
        return disciplina;
    }
}
