package horarios;

import java.util.List;

/**
 *
 * @author hyago
 */
public class Professor {

    private String nome;
    private List<Preferencia> preferencias;

    public Professor(String nome) {
        this.nome = nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setPreferencias(List<Preferencia> preferencias) {
        this.preferencias = preferencias;
    }

    public String getNome() {
        return nome;
    }
    
    public List<Preferencia> getPreferencias() {
        return preferencias;
    }
}
